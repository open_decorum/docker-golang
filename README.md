# Docker Golang

Custom Go Docker image with project dependencies already installed.

Based off the offical [Go Docker image][dkr-go].

[dkr-go]: https://github.com/docker-library/golang

## Pulling

To pull this docker image, run:

    docker pull registry.gitlab.com/open_decorum/docker-golang:1.12-buster

## Running

Example of running this image:

    docker run --rm -ti registry.gitlab.com/open_decorum/docker-golang:1.12-buster /bin/bash

## CI/CD

This image is meant to be used as a part of CI/CD for other projects using the
Gitlab Runner Docker executor.

Example of using this in a `.gitlab-ci.yml`:

```yaml
stages:
  - lint

.use-golang-image: &use-golang
  image: registry.gitlab.com/open_decorum/docker-golang:1.12-buster
  before_script:
    - go mod download

lint:
  <<: *use-golang
  stage: lint
  services:
  script:
    - mage lint
```

## Building

To build the image locally, run the following from the project root:

    make build
