.PHONY: build

NAME ?= "registry.gitlab.com/open_decorum/docker-golang"

help:
	@echo 'Usage: make <target>'
	@echo ''
	@echo 'Targets:'
	@echo ''
	@echo '  build    builds the docker images'
	@echo '  push     pushes the docker images to the registry'
	@echo ''

build:
	docker build -t $(NAME):1.12-buster ./1.12/buster/
	docker build -t $(NAME):1.13-buster ./1.13/buster/

push:
	docker push $(NAME):1.12-buster
	docker push $(NAME):1.13-buster
